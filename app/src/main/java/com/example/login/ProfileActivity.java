package com.example.login;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        //setContentView(R.layout.activity_profile);

        String login = intent.getExtras().getString("login");
        String password = intent.getExtras().getString("password");
        TextView text = new TextView(this);
        text.setTextSize(45f);
        text.setText("Hello," + login + "!" + "\n" + "Your password is:   " + password + "\n" + "Welcome!");
        setContentView(text);
    }

}

