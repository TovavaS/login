package com.example.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bRegister;
    EditText etLogin, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bRegister = (Button) findViewById(R.id.b_register);
        etLogin = (EditText) findViewById(R.id.et_login);
        etPassword  = (EditText) findViewById(R.id.et_password);

        bRegister.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("login", etLogin.getText().toString());
        intent.putExtra("password", etPassword.getText().toString());
        if(Checker.kolvo(etLogin.getText().toString())&&(Checker.kolvo(etPassword.getText().toString()))&&(Checker.niz(etPassword.getText().toString()))&&(Checker.verx(etPassword.getText().toString()))&&(Checker.sim(etPassword.getText().toString())))startActivity(intent);
        else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "в логине должны присутствовать:" +"\n"+
                            "-больше 2-х букв" +"\n"+
                            "в пароле должны присутствовать:" +"\n"+
                            "-больше 2-х букв" +"\n"+
                            "-буквы нижнего регистра"+ "\n"+
                            "-буквы верхнего регистра" +"\n"+
                            "-символы" + "\n" + "(можно использовать:" +"\n" + "! # $ % & ' () * + , - ." + ")",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0 , 0);
            toast.show();
        }

    }
}
